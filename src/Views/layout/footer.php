
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="jquery.paginate.js"></script>

<script>
    function enablePaginate() {
        $('#pokemon-holder').paginate({
            'perPage': 52,
            'autoScroll': false,
            'scope': $('.pokemon-single')
        });
    }

    function disablePaginate() {
        if (typeof $('#pokemon-holder').data('paginate') !== 'undefined') {
            $('#pokemon-holder').data('paginate').kill();
        }
    }

    $(document).ready(function() {
        $('.search').on('click', function () {
            disablePaginate();
            $('.pokemon-single').hide();
        }).on('mouseout', function () {
            if ($(this).val().length === 0) {
                enablePaginate();
            }
        }).on('keyup', function () {
            if ($(this).val().length > 0) {
                $('.pokemon-single').hide().filter("[data-pokemon*='" + $(this).val().toLowerCase() + "']").show();
                disablePaginate();
            } else {
                $('.pokemon-single').show();
                enablePaginate();
            }
        });

        enablePaginate();
    });
</script>
</body>
</html>