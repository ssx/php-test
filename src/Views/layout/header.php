<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="jquery.paginate.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <title>Pokedex Browser</title>
</head>
<body>
<div class="collapse bg-inverse" id="navbarHeader">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 py-4">
                <h4 class="text-white">Pokedex Browser</h4>
                <p class="text-muted">This is a sample pokedex browser.</p>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-inverse bg-inverse">
    <div class="container d-flex justify-content-between">
        <a href="index.php" class="navbar-brand">Pokedex Browser</a>

        <p class="float-right">
            <br><a href="index.php">All Pokemon</a>
        </p>
    </div>
</div>
