<?php
// We'll just use PHP as it's original purpose - a templating engine. Much
// rather use blade or something similar.
require "layout/header.php";
?>
<div class="album text-muted">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><?php echo ucwords($name); ?></h2>
            </div>
        </div>

        <div class="row">
            <?php
            foreach ($pokemon['sprites'] as $image_type => $image_path) {
                if (!is_null($image_path)) {
                    echo '<div class="col-1"><img src="'.$image_path.'" alt="'.$name.'"></div>';
                }
            }
            ?>
        </div>

        <br>

        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h4>Basic Information</h4><br>
                <table class="table table-bordered table-zebra">
                    <tr>
                        <td>Name:</td>
                        <td><?php echo ucwords($name); ?></td>
                    </tr>
                    <tr>
                        <td>Pokedex ID:</td>
                        <td><?php echo $pokemon['id']; ?></td>
                    </tr>
                    <tr>
                        <td>Height:</td>
                        <td><?php echo ($pokemon['height'] / 10); ?>m</td>
                    </tr>
                    <tr>
                        <td>Weight:</td>
                        <td><?php echo ($pokemon['weight']/10); ?>kg</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 col-sm-12">
                <h4>Abilities</h4><br>
                <ul>
                    <?php
                    foreach ($pokemon["abilities"] as $ability) {
                        echo '<li>'.$ability["ability"]["name"].'</li>';
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12">
                <h4>Species</h4><br>
                <!-- If I'd have taken more time here it would of been nice to pull in the full species data -->
                <ul>
                    <li><?php echo $pokemon["species"]['name']; ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
require "layout/footer.php";
