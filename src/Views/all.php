<?php
// We'll just use PHP as it's original purpose - a templating engine. Much
// rather use blade or something similar.
require "layout/header.php";

echo '<div class="container">';
echo '<input class="form-control search" placeholder="Search for Pokemon..">';
echo '</div>';

echo '<div class="album"><div class="container"><div class="row" id="pokemon-holder">';

foreach ($pokemons as $name => $details) {
    echo '<div class="col-2 text-center pokemon-single" data-pokemon="'.$name.'">';
    echo '<img class="pokemon-img" src="'.($details['sprites']['front_default'] ?? 'http://via.placeholder.com/96x96?text=No Image').'"
                alt="'.$name.'">';
    echo '<p class="card-text">';
    echo '<a href="?name='.$name.'">'.ucwords($name);
    echo '</p><br>';
    echo '</div>';
}

echo '</div></div></div>';

require "layout/footer.php";
