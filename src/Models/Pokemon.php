<?php

namespace App\Models;

/**
 * Pokemon model to query and retrieve Pokemon details. Storage is performed
 * completely by the PokemonCache class.
 *
 * Class Pokemon
 * @package App\Models
 */
class Pokemon
{

    /**
     * @var mixed
     */
    private $data;

    /**
     * Pokemon constructor.
     * @throws \App\Exceptions\BadConfigException
     */
    public function __construct()
    {
        // If the cache file isn't present, we can't create an instance of this model.
        if (!@file_get_contents('../'.getenv('CACHE_FILE'))) {
            throw new \App\Exceptions\BadConfigException('Cache file not set.');
        }

        // Load the cache file decode into an array
        $this->data = json_decode(file_get_contents('../'.getenv('CACHE_FILE')), true);

        // Sort by name
        ksort($this->data);
    }


    /**
     * Return all known Pokemon
     * @param int $offset
     * @param int $limit
     * @return array|mixed
     */
    public function all($offset = 0, $limit = 1000)
    {
        return array_slice($this->data, $offset, $limit);
    }
    

    /**
     * Return a single Pokemon by name
     *
     * @param $name
     * @return array
     * @throws \App\Exceptions\PokemonNotFound
     */
    public function get($name) : array
    {
        // If a Pokemon of this name exists, return it.
        if (array_key_exists($name, $this->data)) {
            $this->name = $name;
            return $this->data[$name];
        }

        // Otherwise we'll fail.
        throw new \App\Exceptions\PokemonNotFound('The requested Pokemon does not exist.');
    }


    /**
     * Search for Pokemons by input string
     *
     * @param $name
     * @return array
     */
    public function find($name)
    {
        return array_filter($this->data, function ($key) use ($name) {
            return (strpos($key, $name) !== false);
        }, ARRAY_FILTER_USE_KEY);
    }


    /**
     * Get a total count of all the Pokemon.
     *
     * @return int
     */
    public function total()
    {
        return count($this->data);
    }
}
