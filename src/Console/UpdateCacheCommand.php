<?php
namespace App\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class UpdateCacheCommand extends Command
{
    protected function configure()
    {
        $this->setName('cache:update')
             ->setDescription('Update the internally cached Pokemon data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Building Pokemon data cache...'.PHP_EOL);

        $cache = new \App\Services\PokemonCache();

        // If the cache file doesn't exist, we need to create it and populate it.
        if (!file_exists(getenv("CACHE_FILE"))) {
            // Fetch all Pokemon from the API
            $pokemons = $cache->getBasicPokemonData();
        } else {
            $pokemons = json_decode(file_get_contents(getenv("CACHE_FILE")), true);
        }

        $progressBar = new ProgressBar($output, count($pokemons));
        $progressBar->start();

        // Iterate each pokemon and get the extended data we need. We could
        // perform this differently using guzzle and run a whole bunch of
        // concurrent requests but that's not fair on the external API.
        foreach ($pokemons as $pokemon => $existing) {
            // Don't process any that we've already cached.
            if (array_key_exists('abilities', $existing)) {
                $progressBar->advance();
                continue;
            }

            // Query the external API by ID to get the additional data
            $extended = $cache->getPokemonById($existing["id"]);

            // Replace the array entry with the full data for this Pokemon
            $pokemons[$pokemon] = array_merge(
                $existing,
                [ 'abilities' => $extended['abilities']],
                [ 'height' => $extended['height']],
                [ 'weight' => $extended['weight']],
                [ 'species' => $extended['species']],
                [ 'sprites' => $extended['sprites']]
            );

            // Write our changes to the JSON file one by one so that if we die for
            // whatever reason, we can just pick up from where we need to afterwards.
            file_put_contents(getenv("CACHE_FILE"), json_encode($pokemons));

            // Show that we're progressed
            $progressBar->advance();

            // Be kind to the Pokemon API
            sleep(0.5);
        }

        // Write all of our data to a JSON file
        file_put_contents(getenv("CACHE_FILE"), json_encode($pokemons));
        $progressBar->finish();
    }
}
