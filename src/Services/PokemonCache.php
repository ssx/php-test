<?php namespace App\Services;

use App\Exceptions\ApiRequestFailedException;
use App\Exceptions\CacheNotPresent;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * Class PokemonCache
 * @package App\Services
 */
class PokemonCache
{

    /**
     * @var
     */
    private $pokemons;


    /**
     * Generic handler to make an API call to the Pokemon API.
     *
     * @param $method
     * @param $endpoint
     * @param array $params
     * @param string $body
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function apiCall($method, $endpoint, $params = [], $body = ''): array
    {

        try {
            $client = new Client();
            $request = new Request($method, getenv("BASE_API_URL") . $endpoint, $params, $body);
            $response = $client->send($request);
            return json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            die($e->getMessage());
            return [];
        }
    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTotalPokemon(): int
    {
        // Make an an API call to get a single Pokemon
        $request = $this->apiCall("GET", "pokemon/?limit=1");

        // Check return has been decoded alright and is an array
        if (!is_array($request) || (count($request) === 0)) {
            return 0;
        }

        // Now return the total count of Pokemon
        return $request["count"];
    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBasicPokemonData(): array
    {
        // Make an an API call to get a single Pokemon
        $request = $this->apiCall("GET", "pokemon/?limit=" . $this->getTotalPokemon());

        $pokemons = [];
        if (count($request["results"]) === 0) {
            return [];
        }

        // We can key this array with the name of the pokemon, that way when
        // we want to look up on it, it'll be faster.
        foreach ($request["results"] as $pokemon) {
            $pokemons[$pokemon["name"]] = [
                "id" => str_replace(
                    "https://pokeapi.co/api/v2/pokemon/",
                    "",
                    substr($pokemon["url"], 0, -1)
                ),
                "url" => $pokemon["url"]
            ];
        }

        // Store our pokemon because we need to go fetch some more data next
        $this->pokemons = $pokemons;

        return $this->pokemons;
    }


    /**
     * @throws CacheNotPresent
     * @throws \App\Exceptions\BadConfigException
     */
    public function checkCacheExists()
    {
        // Check whether we have a cache file. If not, build it.
        if (getenv("CACHE_FILE") == '') {
            throw new \App\Exceptions\BadConfigException('Cache file not set.');
        }

        // If the cache file doesn't exist, we need to create it and populate it.
        if (!file_exists("../".getenv("CACHE_FILE"))) {
            throw new \App\Exceptions\CacheNotPresent('Please run the cache tool: 
                                                                php application.php cache:update');
        }
    }


    /**
     * Query the Pokemon API for a specific pokemon by name.
     *
     * @param $name
     *
     * @return array
     * @throws \App\Exceptions\ApiRequestFailedException
     * @throws \App\Exceptions\BadConfigException
     * @throws \App\Exceptions\PokemonNotFound
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPokemonByName($name)
    {
        $pokemon = (new \App\Models\Pokemon)->get($name);
        $data = $this->apiCall('GET', 'pokemon/' . $pokemon["id"] . '/');

        if (!is_array($data)) {
            throw new ApiRequestFailedException('Could not fetch Pokemon extended data.');
        }

        return $data;
    }


    /**
     * Query the Pokemon API for a specific pokemon by ID.
     *
     * @param $id
     *
     * @return array
     * @throws ApiRequestFailedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPokemonById($id)
    {
        $data = $this->apiCall('GET', 'pokemon/' . $id . '/');

        if (!is_array($data)) {
            throw new ApiRequestFailedException('Could not fetch Pokemon extended data.');
        }

        return $data;
    }


    /**
     * Write our cache file to disk.
     *
     * @param $data
     */
    public function writeCache($data)
    {
        // Write our cache file
        file_put_contents(getenv("CACHE_FILE"), json_encode($data));
    }
}
