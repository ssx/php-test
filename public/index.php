<?php
// Load our composer autoload
require dirname(__FILE__)."/../vendor/autoload.php";

// Generic exception handler to print out a pretty message on error.
function exception_handler($exception) {
    echo "A fatal error occurred:<br><br>" , $exception->getMessage(), "\n";
    exit();
}
set_exception_handler('exception_handler');


// Load our env variables
$dotenv = new Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

// Check that we at least have a cache file present otherwise our UI is useless
$pokemon = new \App\Services\PokemonCache();
$pokemon->checkCacheExists();

// For speed, I've built this page out in the index.php here. I toyed with the
// idea of adding nikic/FastRoute here as at least that would of been easier to
// build out some controllers.
$view = 'start.php';

// If any bad params were passed, reset to sane values here. We don't need to reset
// start as it'll be page 0 and that's a pretty good place to start.
$limit = (int)$_GET["limit"];
$offset = (int)$_GET["offset"];
if ($limit === 0) $limit = 60;

// We're looking for a specific Pokemon
if ($_GET["name"]) {
    $name = preg_replace('/[^a-z_\-0-9]/i', '', strtolower($_GET["name"]));
    $view = 'single.php';

    // Get our single Pokemon
    $pokemon = (new \App\Models\Pokemon())->get($name);

    // Display our view
    require "../src/Views/single.php";
    exit();
}

// This could be used to perform the search on the PHP side of things, however in the end
// I went with jQuery/data attributes instead. Left this here as a possible for the future.
if ($_GET["search"]) {
    $query = preg_replace('/[^a-z_\-0-9]/i', '', strtolower($_GET["search"]));

    // No view to display, so we'll send JSON out
    Header("Content-Type: application/json");
    echo json_encode([ 'matches' => (new \App\Models\Pokemon())->find($query)] );
    exit();
}


// If we're here then we're listing all pokemon with some nice pagination.
$view = 'all.php';
$pokemons = (new \App\Models\Pokemon())->all();

$viewPath = "../src/Views/".$view;
if (!file_exists($viewPath))
    throw new Exception('Provided view does not exist.');

// We have a view that we can display, so do so and then exit cleanly.
require "../src/Views/" . $view;
exit();
