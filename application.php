<?php
require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;

// Load our env variables
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$application = new Application();
$application->add(new \App\Console\UpdateCacheCommand);
$application->run();
