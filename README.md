# ukfast/pokédex

This application contains a CLI comoponent to update the cache. You can run it
with the following command:

```php application.php cache:update```

Which will run through and cache the Pokemon data required for the app to run.
